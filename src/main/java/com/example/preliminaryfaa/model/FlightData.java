package com.example.preliminaryfaa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.time.LocalDate;

public class FlightData {
    @JsonProperty("year_product_no")
    private String id;
    @JsonProperty("file_name")
    private String fileName;
    @JsonProperty("effective_date")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate effectiveDate;
    private String size;

    public String getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public String getSize() {
        return size;
    }
}
