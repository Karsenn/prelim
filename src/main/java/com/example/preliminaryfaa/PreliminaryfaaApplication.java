package com.example.preliminaryfaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreliminaryfaaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PreliminaryfaaApplication.class, args);
    }

}
