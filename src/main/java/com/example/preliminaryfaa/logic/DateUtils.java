package com.example.preliminaryfaa.logic;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {
    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final static Integer CYCLE_LENGTH = 56;

    public static LocalDate parse(String fileName) {
        String date = null;

        if (fileName.startsWith("nfdd")) {
            date = fileName.split("nfdd-")[1].split("[.]")[0].substring(0, 10);
        } else if (fileName.startsWith("transmittal_letter")) {
            date = fileName.split("transmittal_letter-")[1].split("[.]")[0].substring(0, 10);
        } else {
            throw new UnsupportedOperationException("Wrong file taken.");
        }
        return LocalDate.parse(date, FORMATTER);
    }

    public static boolean isInCycle(String filename) {
        LocalDate today = LocalDate.now();
        LocalDate periodStartDate = today.minusDays(CYCLE_LENGTH);

        LocalDate date = parse(filename);
        return date.isAfter(periodStartDate);
    }
}
