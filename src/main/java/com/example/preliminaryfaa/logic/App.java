package com.example.preliminaryfaa.logic;

import com.example.preliminaryfaa.model.FlightData;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

public class App {

    static {
        File dataFolder = new File("./data");
        if (!dataFolder.exists() || !dataFolder.isDirectory()) {
            if (dataFolder.mkdir()) {
                System.out.println("Directory created.");
            }
        }
    }

    private static int numberOfNfdd;
    private static int numberOfTrasmittalLetter;

//    public static void main(String[] args) throws IOException {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Enter the number of NFDD files");
//        App.numberOfNfdd = scanner.nextInt();
//        System.out.println("Enter the number of Transmittal Letter files");
//        App.numberOfTrasmittalLetter = scanner.nextInt();
//
//        if (App.numberOfNfdd <= 0 || App.numberOfNfdd > 40 || App.numberOfTrasmittalLetter <= 0 || App.numberOfTrasmittalLetter > 20) {
//            System.out.println("Incorrect value, try again");
//            App.numberOfNfdd = scanner.nextInt();
//            App.numberOfTrasmittalLetter = scanner.nextInt();
//        }
//
//        run(numberOfNfdd, numberOfTrasmittalLetter);
//    }

    public static void run(int nfdd, int trasmitalLetter) throws IOException {
        try {
            downloadTransmitallLetterPdf();
            downloadNfddPdf();
        } catch (IOException ioe) {
            Logger.getLogger(App.class.getName()).severe("Error loading files from server. Passing with what is on the drive.");
        }
        File file = new File("src/main/java/resources/currentPrelim.csv");
    }

}
