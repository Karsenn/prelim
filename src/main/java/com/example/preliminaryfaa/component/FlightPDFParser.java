package com.example.preliminaryfaa.component;

import com.example.preliminaryfaa.logic.App;
import com.example.preliminaryfaa.logic.DateUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

@Component
public class FlightPDFParser {
    private List<String> readFile(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        List<String> array = new ArrayList<>();
        int counter = 0;
//        Poniżej usuwam przedrostki SID i STAR oraz wszystkie wyrazy RNAV i OBSTACLE, a także wszystkie nawiasy.
//        W związku z tym, że czasami występuję więcej niż jedna spacja pomiędzy wyrazami zastosowałem zamianę pustych
//        znaków na dokładnie jeden pusty znak


        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
//          Tutaj pozbywam się wszsytkich linii zawierających ORIG lub AMDT oraz pierwszą linię tytułową poprzez wywołanie
//          dowolnego pola.
            if (line.contains("ORIG") || line.contains("AMDT") || line.contains("ICAO ID")) {
                continue;
            }
            line = line.replaceAll("[0-9]", "");
            line = line.replaceAll("SID", "");
            line = line.replaceAll("STAR", "");
            line = line.replaceAll("\\(", "");
            line = line.replaceAll("\\)", "");
            line = line.replaceAll("OBSTACLE", "");
            line = line.replaceAll("RNAV", "");
            line = line.replaceAll("\\s+", " ");
//            Następnie zamieniam wszystkie wyrazy na cyfry
            if (line.contains(" ONE ")) line = line.replaceAll(" ONE ", "1 ");
            else if (line.contains(" TWO ")) line = line.replaceAll(" TWO ", "2 ");
            else if (line.contains(" THREE ")) line = line.replaceAll(" THREE ", "3 ");
            else if (line.contains(" FOUR ")) line = line.replaceAll(" FOUR ", "4 ");
            else if (line.contains(" FIVE ")) line = line.replaceAll(" FIVE ", "5 ");
            else if (line.contains(" SIX ")) line = line.replaceAll(" SIX ", "6 ");
            else if (line.contains(" SEVEN ")) line = line.replaceAll(" SEVEN ", "7 ");
            else if (line.contains(" EIGHT ")) line = line.replaceAll(" EIGHT ", "8 ");
            else if (line.contains(" NINE ")) line = line.replaceAll(" NINE ", "9 ");

//            Następnie zamieniam wszystkie stringi na tablicę do której dodaję wyrazy zawierające cyfry.
            String[] slowa = line.split(" ");
            for (String slowo : slowa) {
                if (!slowo.contains("1") && !slowo.contains("2") && !slowo.contains("3") && !slowo.contains("4") && !slowo.contains("5")
                        && !slowo.contains("6") && !slowo.contains("7") && !slowo.contains("8") && !slowo.contains("9")) {
                    continue;
                }
//                if (slowo.length() == 5) {
//                    counter++;
//                    array.add(slowo);
//                    System.out.println(slowo);
//                } else {
                slowo = slowo.substring(0, slowo.length() - 1);
                counter++;
                array.add(slowo);
                System.out.println(slowo);
//                }
            }
        }
        System.out.println("FAA is going to publish " + counter + " procedures this cycle");
        return array;
    }

    private void ReadTextFromPdfFile(List<String> array) throws IOException {
        File dataDirectory = new File("./data");
        Set<String> filteredProcedures = new HashSet<>(array);

        for (File pdfFile : dataDirectory.listFiles()) {
            if (!pdfFile.getName().endsWith(".pdf") || (!pdfFile.getName().startsWith("nfdd") && !pdfFile.getName().startsWith("transmittal_letter"))) {
                Logger.getLogger(App.class.getName()).info("Omijam plik:" + pdfFile.getName());
                continue;
            }

            if (!DateUtils.isInCycle(pdfFile.getName())) {
                Logger.getLogger(App.class.getName()).info("Omijam plik (poza cyklem):" + pdfFile.getName());
//                pdfFile.delete();
                continue;
            }

            PDDocument document = PDDocument.load(new File("data/" + pdfFile.getName()));
            String[] podzielone = {""};
            int counter = 0;

            if (!document.isEncrypted()) {
                PDFTextStripper stripper = new PDFTextStripper();
                String text = stripper.getText(document);
//                if(text.contains("NUMBER") || text.contains("NAME")){
//                    System.out.println("found it");
//                }
                String[] slowa = text.split(" ");


                for (String slowo : slowa) {
                    for (int i = 0; i < (array.size()); i++) {
//                        if(slowo.contains("NUMBER") || slowo.contains("NAME")){
//                            System.out.println("found it");
//                        }
                        if (slowo.contains("\n") && slowo.contains(array.get(i))) {
                            podzielone = slowo.split("\n");
                            for (int j = 0; j < podzielone.length; j++) {

                                if ((podzielone[j].contains("Date") || podzielone[j].contains("Name") || podzielone[j].contains("NAME")) &&
                                        podzielone[1].contains(array.get(i))) {

                                    filteredProcedures.remove(array.get(i));
                                    counter++;
                                    System.out.println(array.get(i));
                                }
                            }
                        }
                    }
                }

            }
            document.close();
            System.out.println(counter);
        }

        System.out.println("Filtered procedures: " + filteredProcedures);
        System.out.println("FAA published " + (array.size() - filteredProcedures.size()) +
                " procedures which is " + (array.size() - filteredProcedures.size()) * 100 / array.size() + "% of total expected procedures");
    }

}
