package com.example.preliminaryfaa.service;

import com.example.preliminaryfaa.logic.App;
import com.example.preliminaryfaa.model.FlightData;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

@Service
public class NfddDownloader {
//      updateFiles (jak metoda run z App)
// te dwie metody (wywolanie): downloadTransmitallLetterPdf(); downloadNfddPdf();

    public void updateFiles(int nfdd, int trasmitalLetter, File dataFolder) throws IOException {
        dataFolder = new File("./data");
        if (!dataFolder.exists() || !dataFolder.isDirectory()) {
            if (dataFolder.mkdir()) {
                System.out.println("Directory created.");
            }
        }
        try {
            downloadTransmitallLetterPdf();
            downloadNfddPdf();
        } catch (IOException ioe) {

            Logger.getLogger(App.class.getName()).severe("Error loading files from server. Passing with what is on the drive.");
        }
        File file = new File("src/main/java/resources/currentPrelim.csv");
    }


//    A to przenies do konstruktora
//static {
//    File dataFolder = new File("./data");
//    if (!dataFolder.exists() || !dataFolder.isDirectory()) {
//        if (dataFolder.mkdir()) {
//            System.out.println("Directory created.");
//        }
//    }
//}

    private void downloadNfddPdf() throws IOException {
        URL url = new URL("https://nfdc.faa.gov/nfdcApps/controllers/PublicDataController/getNfddListData?dataType=NFDD&start=0&length=" + numberOfNfdd + "&sortcolumn=effective_date&sortdir=desc");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        try (InputStream inputStream = url.openStream()) {
            ObjectNode node = objectMapper.readValue(inputStream, ObjectNode.class);
            JsonNode data = node.get("data");

            JsonParser parser = objectMapper.treeAsTokens(data);
            TypeReference<List<FlightData>> rootType = new TypeReference<List<FlightData>>() {
            };
            List<FlightData> nationalFlightDatas = objectMapper.readValue(parser, rootType);

            for (FlightData nationalFlightData : nationalFlightDatas) {
                System.out.println(nationalFlightData.getEffectiveDate() + " : " + nationalFlightData.getFileName());


                URL url1 = new URL("https://nfdc.faa.gov/webContent/nfdd/" + nationalFlightData.getFileName());
                InputStream in = url1.openStream();
                FileOutputStream fos = new FileOutputStream(new File("data/" + nationalFlightData.getFileName()));
                int length;
                byte[] buffer = new byte[1024];// buffer for portion of data from connection
                while ((length = in.read(buffer)) > -1) {
                    fos.write(buffer, 0, length);
                }
            }
            parser.close();
        }
    }

    private void downloadTransmitallLetterPdf() throws IOException {

        URL url = new URL("https://nfdc.faa.gov/nfdcApps/controllers/PublicDataController/getTransmittalLetterListData?dataType=TRANSMITTALLETTER&start=0&length=" + numberOfTrasmittalLetter + "&sortcolumn=effective_date&sortdir=desc");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        try (InputStream inputStream = url.openStream()) {
            ObjectNode node = objectMapper.readValue(inputStream, ObjectNode.class);
            JsonNode data = node.get("data");

            JsonParser parser = objectMapper.treeAsTokens(data);
            TypeReference<List<FlightData>> rootType = new TypeReference<List<FlightData>>() {
            };
            List<FlightData> transimittalLetters = objectMapper.readValue(parser, rootType);

            for (FlightData transimittalLetter : transimittalLetters) {
                System.out.println(transimittalLetter.getEffectiveDate() + " : " + transimittalLetter.getFileName());


                URL url1 = new URL("https://nfdc.faa.gov/webContent/transmittal_letter/" + transimittalLetter.getFileName());
                InputStream in = url1.openStream();
                FileOutputStream fos = new FileOutputStream(new File("data/" + transimittalLetter.getFileName()));
                int length;
                byte[] buffer = new byte[1024];// buffer for portion of data from connection
                while ((length = in.read(buffer)) > -1) {
                    fos.write(buffer, 0, length);
                }
            }
            parser.close();
        }

    }

    private void downloadCSV() throws IOException {
        try (BufferedInputStream inputStream = new BufferedInputStream(new URL("https://www.faa.gov/air_traffic/flight_info/aeronav" +
                "/procedures/application/index.cfm?event=procedure.exportResults&tab=productionPlan&publicationDate=9%2F10%2F2020%20%28CN" +
                "%29%2C10%2F8%2F2020%20%28TPP%29").openStream());
             FileOutputStream fileOS = new FileOutputStream("src/main/java/resources/currentPrelim.csv")) {
            byte[] data = new byte[1024];
            int byteContent;
            while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                fileOS.write(data, 0, byteContent);
            }
        } catch (IOException e) {
        }
    }
//      downloadCSV - do zrobienia pozniej, metoda bedzie pobierac najnowsze csv i wrzucac na dysk
//      zamiast powyzszej zrob: loadCSV - laduje csv z dysku
}